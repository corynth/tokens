# Corynth Discord Bot Tokens Repo
This is the repository where tokens that are found in discord servers (with token removing on) are sent.<br>
If you are here from a discord notification, note that this is not malicious, and it is designed to stop tokens being used in malicious ways if it were to be accidentally sent in a server.<br>
If you wish to turn this feature off in your server, please run `@Corynth token-remover`<br>
If you have an issue with this repository, please make an issue so it can be discussed.